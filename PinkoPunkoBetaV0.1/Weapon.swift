//
//  Weapon.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 6/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import Foundation
import SpriteKit

class Weapon: SKSpriteNode {
    
    private var levelRequirement: Int!
    private var itemLevel: Int!
    private var rarity: Int!
    
    private var minMeleeDmg: Int!
    private var maxMeleeDmg: Int!
    private var minMagicDmg: Int!
    private var maxMagicDmg: Int!
    private var critChance: Int!
    
    private var fire: Bool = false
    private var ice: Bool = false
    private var wind: Bool = false
    private var electricity: Bool = false
    private var water: Bool = false
    
    var texturesIdle = [SKTexture]()
    var texturesAttack = [SKTexture]()
    
    
    init(name: String, texture: String, size: CGSize, levelRequirement: Int, itemLevel: Int, rarity: Int) {
        let weaponTexture = SKTexture(imageNamed: texture)
        super.init(texture: weaponTexture, color: UIColor.whiteColor(), size: size)
        self.name = name
        self.levelRequirement = levelRequirement
        self.itemLevel = itemLevel
        self.rarity = rarity
        
        minMeleeDmg = itemLevel * 5
        maxMeleeDmg = itemLevel * 6
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // set the elemental type
    func setFire(){
        fire = true
    }
    
    func setIce(){
        ice = true
    }
    
    func setWind(){
        wind = true
    }
    
    func setElectricity(){
        electricity = true
    }
    
    func setWater(){
        water = true
    }
    
    func runIdleAnimation(){
    }
    
    func runAttackAnimation(){
    }
    
}