//
//  GameScene.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 5/07/2016.
//  Copyright (c) 2016 Sam. All rights reserved.
//

import SpriteKit

/*
 A scene calculates its updates in the following order (happens b/t each frame):
 - first the "-update:" method is called.
 - SKScene evaluates all actions for the children
 - "-didEvaluateActions"
 - SKScene simulates physics
 - "-didSimulatePhysics"
 - SKScene applies contraints
 - "-didApplyConstraints"
 - "-didFinishUpdate"
 - SKScene renders the scene
 */


// MARK: GameState ENUM
// 3 different Game states:
// when the game is ready, running and over
enum GameState {
    case ready
    case playing
    case gameOver
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    // MARK: Fields
    private var backgroundLayer = BackgroundLayer()
    private var worldLayer = WorldLayer()
    private var gameLayer = Layer()
    private var hudLayer = SKNode()
    private var groundLayer = SKNode()
    
    private var joyPad: JoypadScene!
    
    // initially, our game state will be on "ready" i.e. paused via the enum
    private var gameState = GameState.ready

    // note: exclamation marks after the field's data type means we haven't initialised it yet, but it WILL AND MUST be
    private var hero: Hero!
    private var heroInitPosition: CGPoint!
    
    // all the adjacent levels
    private var adjacentScenes: Array<GameScene> = Array<GameScene>()
    
    // variable to determine if the character has made the 1st or 2nd jump
    private var jump1: Bool = false
    private var jump2: Bool = false
    
    // variable to determine which way we were facing
    private var right: Bool = true
    private var left: Bool = false
    
    // variable to let us know if we're moving or not
    private var idle: Bool = true
    private var movingSlow: Bool = false
    private var movingFast: Bool = false
    
    // for ongoing contact between hero and enemies
    private var currTime: NSTimeInterval = 0
    private var touchBegan: NSTimeInterval = 0
    private var numTouching: Int = 0
    private var stillTouching: Bool = false
    private var stillTouchingDmg: Int = 0
    
    
    // MARK: Init
    // Constructor
    init(initName: String, initHero: Hero, initSize: CGSize, initPosition: CGPoint, joypadScene: JoypadScene){
        super.init(size: initSize)
        self.name = initName
        self.hero = initHero
        self.hero.position = initPosition
        self.heroInitPosition = initPosition
        /* Set the scale mode to scale to fit the window */
        self.scaleMode = .AspectFill
        connectJoypad(joypadScene)
        
        // add the layers only once (and not every time we enter the view)
        addLayers()
        addPhysics()
        setCamera()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Move From/To View
    // entering the Scene
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        gameState = .ready
        // add our hero to this level upon entering the screen. We cannot have the hero with multiple parents so upon dying or entering another scene we must remove the hero from it's parents within it's current scene
        addHero()
        
        // FLUSH
        // variable to determine if the character has made the 1st or 2nd jump
        jump1 = false
        jump2 = false
        
        // variable to determine which way we were facing
        right = true
         left = false
        
        // for ongoing contact between hero and enemies
        currTime = 0
        touchBegan = 0
        numTouching = 0
        stillTouching = false
        stillTouchingDmg = 0
        
        // init the camera position
        camera?.position = hero.position
    }
    
    // leaving the Scene
    override func willMoveFromView(view: SKView) {
        // remove the hero from it's parents (i.e. this scene)
        hero.removeFromParent()
    }
    
    
    
    // MARK: Touch UI
    // touch began
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
//        for touch in (touches){
//            let location = touch.locationInNode(self)
//        }
        
        print("touchBegan")
        
        // change game states upon touch
        switch gameState {
            
        case .ready:
            paused = false
            gameState = .playing
            
        case .playing:
//                print("playing \(touch)")
            break
            
        case .gameOver:
            // pause everytime we die
            paused = true
            
        }
        print("touchBegan -end")
        
        
       
    }

    
    // MARK: Update
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
//        print(currentTime)
        currTime = currentTime
        
        // checks if game state is still ongoing (i.e. not gameOver)
        if gameState == .playing {
            
            // joypad movement
            if joyPad.isActive() {
                self.move(joyPad.getBallxPos())
            }
            
            // checks if hero is still touching an enemy
            if currTime >= touchBegan + 2 && numTouching > 0 {
                self.damage(stillTouchingDmg)
                touchBegan = currentTime
                hero.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
                hero.physicsBody?.applyImpulse(CGVector(dx: 10,dy: 30))
            }
            
            // checks the direction to show the enemy
            if hero.physicsBody?.velocity.dx > 0{
                right = true
                left = false
            } else if hero.physicsBody?.velocity.dx < 0{
                right = false
                left = true
            }
            
            // to create a more dynamic moving pattern
            // if our hero is in the air
            if jump1 || jump2 {
                
                // play jumping animation
                hero.runJumpAnimation()
                
                // if our hero is going upwards
                if hero.physicsBody?.velocity.dy > 0 {
                    if right {
                        
                        hero.xScale = 1.0
                    } else if left {
                        hero.xScale = -1.0
                    }
                    
                // else if is going down
                } else {
                    if right {
                        hero.xScale = 1.0
                    } else if left {
                        hero.xScale = -1.0
                    }
                }
            // if moving on the ground
            } else {
                
                // check if we're idle or not
                idle = hero.physicsBody?.velocity.dx == 0 ? true : false
                
                // if we're not idle anymore (and we "weren't" moving before)
                if !movingSlow && !movingFast && !idle {
                    // check if we're moving quickly or not
                    if hero.physicsBody?.velocity.dx >= 250 || hero.physicsBody?.velocity.dx <= -250 {
                        hero.runMovingAnimation(true)
                        print ("fast")
                        movingSlow = false
                        movingFast = true
                    } else {
                        hero.runMovingAnimation(false)
                        print ("slow")
                        movingSlow = true
                        movingFast = false
                    }
                    // if we're currently moving slowly, check if we're moving quickly
                } else if movingSlow && !movingFast && !idle {
                    if hero.physicsBody?.velocity.dx >= 250 || hero.physicsBody?.velocity.dx <= -250 {
                        hero.runMovingAnimation(true)
                        print ("fast")
                        movingSlow = false
                        movingFast = true
                    }
                    // if we're currently moving fast, check if we're moving slowly
                } else if !movingSlow && movingFast && !idle {
                    if hero.physicsBody?.velocity.dx < 250 && hero.physicsBody?.velocity.dx > -250 {
                        hero.runMovingAnimation(false)
                        print ("slow")
                        movingSlow = true
                        movingFast = false
                    }
                    
                    // else if we're now idle, but was previously moving
                } else if idle && (movingSlow || movingFast){
                    hero.runIdleAnimation()
                    print ("idle")
                    movingSlow = false
                    movingFast = false
                }
                
                // if the hero is moving horizontally, set idle to false
                idle = hero.physicsBody?.velocity.dx == 0 ? true : false
                
                if right {
                    hero.xScale = 1.0
                } else if left {
                    hero.xScale = -1.0
                }
            }
            
        } else if gameState == .ready {
            paused = true
        } else if gameState == .gameOver {
            hero.runDeathAnimation()
        }
        
    }
    
    // end of each update
    override func didFinishUpdate() {
        camera?.position = hero.position
        if hero.getHeroHealth() <= 0 {
            gameState = .gameOver
            hero.set_hero_dead()
        }
    }
    
    // MARK: Contact Check
    func didBeginContact(contact: SKPhysicsContact) {
        
        // if contact between the hero and the ground occurs
        if contact.bodyA.categoryBitMask == heroCategory && contact.bodyB.categoryBitMask == groundCategory || contact.bodyA.categoryBitMask == groundCategory && contact.bodyB.categoryBitMask == heroCategory {
            
            print ("yay")
            // re-enable the hero's ability to jump again (since the hero is back on the ground)
            self.jump1 = false
            self.jump2 = false
            if right {
                hero.xScale = 1.0
            } else if left {
                hero.xScale = -1.0
            }
            
            
        // if contact between the hero and enemy occurs
        } else if contact.bodyA.categoryBitMask == heroCategory && contact.bodyB.categoryBitMask == enemyCategory || contact.bodyA.categoryBitMask == enemyCategory && contact.bodyB.categoryBitMask == heroCategory {
            
            //set the game state to "gameOver"
//            gameState = .gameOver
            print ("contact with enemy")
            print("didBeginContact: \(numTouching)")
            numTouching += 1
            
            // conditional cast!!!
            if let enemy = contact.bodyA.node as? Enemy {
                self.stillTouchingDmg = enemy.getEnemyDmg()
            } else if let enemy = contact.bodyB.node as? Enemy {
                self.stillTouchingDmg = enemy.getEnemyDmg()
            }
            
            // if we're not immune, i.e.
            // if we haven't waited long enough
            if currTime >= touchBegan + 2 {
                self.damage(stillTouchingDmg)
                self.touchBegan = currTime
                hero.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
                if (contact.bodyA.categoryBitMask == heroCategory && contact.bodyA.node?.position.x >= contact.bodyB.node?.position.x ) {
                    hero.physicsBody?.applyImpulse(CGVector(dx: 10,dy: 30))
                } else {
                    hero.physicsBody?.applyImpulse(CGVector(dx: -10,dy: 30))
                }
            }
            
        }
        
    }
    
    // end of contact
    func didEndContact(contact: SKPhysicsContact) {
        // if contact between the hero and enemy ends
        if contact.bodyA.categoryBitMask == heroCategory && contact.bodyB.categoryBitMask == enemyCategory || contact.bodyA.categoryBitMask == enemyCategory && contact.bodyB.categoryBitMask == heroCategory {
            numTouching -= 1
            print("didEndContact: \(numTouching)")
        }
    }
    
    
    
    
    
    
    func jump(){
        // checks if we're currently in jump1 or jump2
        if !jump1 {
            // makes the hero jump upon touch
            
            hero.physicsBody?.velocity = CGVector(dx: (hero.physicsBody?.velocity.dx)!, dy: 0.0)
            hero.physicsBody?.applyImpulse(CGVector(dx: 0.0 ,dy: 200.0))
            print ("j1")
            jump1 = true
        } else if !jump2 {
            // makes the hero jump upon touch
            hero.physicsBody?.velocity = CGVector(dx: (hero.physicsBody?.velocity.dx)!, dy: 0.0)
            hero.physicsBody?.applyImpulse(CGVector(dx: 0.0 ,dy: 200.0))
            print ("j2")
            jump2 = true
        }
    }
    
    func attack(){
        // makes the hero attack
        hero.runAttackAnimation()
        
    }
    
    func move(xVel: CGFloat){
        // makes the hero move
//        print("velocity = \(xVel)")
        
        if (xVel) * 1.5 < 100 && (xVel) * 1.5 > 0 {
            hero.physicsBody?.velocity = CGVector(dx: 100, dy: (hero.physicsBody?.velocity.dy)!)
        } else if (xVel) * 1.5 > -100 && (xVel) * 1.5 < 0 {
            hero.physicsBody?.velocity = CGVector(dx: -100, dy: (hero.physicsBody?.velocity.dy)!)
        } else {
            hero.physicsBody?.velocity = CGVector(dx: (xVel) * 1.5, dy: (hero.physicsBody?.velocity.dy)!)
        }
//        print ("\((xVel) * 1.5)")
        
    }
    
    
    func damage(enemyDmg: Int){
        print ("dmg taken")
        print("hero's health was: \(hero.getHeroHealth())")
        hero.takeDamage(enemyDmg)
        print("hero's health now: \(hero.getHeroHealth())")
    }
    
    
    
    
    
    
    // MARK: add Layers
    func addLayers(){
        addChild(backgroundLayer)
        addChild(hudLayer)
        addChild(gameLayer)
        gameLayer.addChild(worldLayer)
        gameLayer.addChild(groundLayer)
        
        addContent()
    }
    
    // MARK: Content
    func addContent(){
        addBackground()
        addGround()
    }
    
    // add the scene which is connected to the current scene
    func addAdjacentScene(scene: GameScene, portalLocation: CGPoint){
        // add to the list of
        adjacentScenes.append(scene)
        createPortal(portalLocation)
    }
    
    // create a portal for the adjacent scene
    func createPortal(portalLocation: CGPoint){
        
    }
    
    // accept actions for entering a portal
    func enterPortal(){
        // a check for the location of hero, location of portal on map, correct action
        
        // replace the current scene with the portal being entered
    }
    
    // MARK: add Background
    func addBackground(){
        // note that the blue reference folders are needed for this, not the orange reference folder
        let background = SKSpriteNode(imageNamed: "Images/Levels/" + self.name! + "/Background/Ground")
        background.xScale = 4
        background.yScale = background.xScale
        background.anchorPoint = CGPointZero
        background.position = CGPointZero
        background.zPosition = 1 // the lower, the futher back
        background.name = "1"
        backgroundLayer.addChild(background)
    }
    
    // MARK: Hero
    func addHero(){
        hero.position = self.heroInitPosition
        hero.zPosition = 9
        hero.runIdleAnimation()
        
        print("add hero")
        worldLayer.addChild(hero)
        print("add hero complete")
        
    }
    
    // MARK: Ground
    func addGround(){
        let ground = SKSpriteNode(color: UIColor.clearColor(), size: CGSize(width: screenWidth, height: 60.0))
        ground.anchorPoint = CGPointZero
        ground.position = CGPointZero
        ground.addPhysicsBodyWithSize(ground.size, gravity: false, dynamic: false, rotation: false, friction: 0.1, restitution: 0.0, categoryBitMask: groundCategory, contactTestBitMask: heroCategory, collisionBitMask: heroCategory)
        
        groundLayer.addChild(ground)
    }
    
    // MARK: Physics
    func addPhysics(){
        // all contact between physical bodies will be handled within the GameScene class
        physicsWorld.contactDelegate = self
    }
    
    // MARK: Enemy
    func addEnemy(enemy: Enemy, enemyPosition: CGPoint){
        enemy.position = enemyPosition
        enemy.zPosition = 2
        worldLayer.addChild(enemy)
    }
    
    // MARK: Camera
    func setCamera(){
        let cameraNode = SKCameraNode()
        cameraNode.position = hero.position
        self.addChild(cameraNode)
        self.camera = cameraNode
    }
    
    func connectJoypad(joypadInput: JoypadScene){
        self.joyPad = joypadInput
    }

    
}
