//
//  Hero.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 5/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import Foundation
import SpriteKit

class Hero: SKSpriteNode {
    // MARK: Fields
    private var hero_level: Int = 1
    
    private var hero_exp_curr: Double = 0.0
    private var hero_exp_toNextLvl: Double = 100.0
    
    private var hero_skills: Array<Skill> = Array<Skill>()
    
    private var hero_hitpoints_max: Int = 100
    private var hero_hitpoints_curr: Int = 100
    
    private var hero_manapoints_max: Int = 100
    private var hero_manapoints_curr: Int = 100
    
    private var hero_melee_dmg_max: Int = 5
    private var hero_melee_dmg_min: Int = 2
    
    private var hero_magic_dmg_max: Int = 5
    private var hero_magic_dmg_min: Int = 2
    
    private var hero_att_speed: Double = 1
    
    private var hero_melee_def: Int = 0
    private var hero_magic_def: Int = 0
    
    private var hero_dead: Bool = false
    
    private var inventory: Inventory!
    private var weaponLayer = WeaponLayer()
    private var armourLayer = ArmourLayer()
    
    private var texturesIdle = [SKTexture]()
    private var texturesMoving = [SKTexture]()
    private var texturesDeath = [SKTexture]()
    private var texturesAttack = [SKTexture]()
    private var texturesJump = [SKTexture]()
    
    init(initLevel: Int, initTexture: String, initSize: CGSize){
        let heroImage = SKTexture(imageNamed: initTexture)
        super.init(texture: heroImage, color: UIColor.whiteColor(), size: initSize)
        self.addPhysicsBodyWithSize(self.size, gravity: true, dynamic: true, rotation: false, friction: 0.1, restitution: 0.0,
                                    categoryBitMask: heroCategory,
                                    contactTestBitMask: groundCategory | enemyCategory | scoreCategory,
                                    collisionBitMask: groundCategory)
        inventory = Inventory()
        
        
        var imgName: String
        
        // Idle animation
        for i in 1...2 {
            imgName = "Images/Hero/Character/Idle/MC_Idle_0\(i)"
            self.texturesIdle.append(SKTexture(imageNamed: imgName))
        }
        
        // Walking animation
        for i in 1...2 {
            imgName = "Images/Hero/Character/Moving/MC_Running_0\(i)"
            self.texturesMoving.append(SKTexture(imageNamed: imgName))
        }
        
        // Death animation
        imgName = "Images/Hero/Character/Death/MC_Death"
        self.texturesDeath.append(SKTexture(imageNamed: imgName))
        
        // Attack Animation
        for i in 1...3 {
            imgName = "Images/Hero/Character/Attack/MC_Attack_0\(i)"
            self.texturesAttack.append(SKTexture(imageNamed: imgName))
        }
        
        // Jump animation
        imgName = "Images/Hero/Character/Jump/MC_Jumping"
        self.texturesJump.append(SKTexture(imageNamed: imgName))
        
        addChild(weaponLayer)
        addChild(armourLayer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    // MARK: Functions
    func performSkill(){
        
    }
    
    func levelUp (){
        hero_level += 1
    }
    
    func addSkill(newSkill: Skill){
        hero_skills.append(newSkill)
    }
    
    func getHeroHealth() -> Int{
        return hero_hitpoints_curr
    }
    
    
    // takes damage
    // change to damage taken animation
    // check if you die
    func takeDamage(damageDone: Int){
        self.hero_hitpoints_curr -= damageDone
        
    }
    
    // hero has died
    // change to death animation
    func set_hero_dead (){
        self.hero_dead = true
    }
    
    func revive_hero(){
        self.hero_hitpoints_curr = 100
        self.hero_dead = false
    }
    
    func pickUpItem (item: SKNode){
        if item.isKindOfClass(Weapon){
            if let itemIsWeapon = item as? Weapon{
                inventory.addWeapon(itemIsWeapon)
                equipWeapon()
            }
        } else if item.isKindOfClass(Armour){
            if let itemIsArmour = item as? Armour {
                inventory.addArmour(itemIsArmour)
                equipArmour()
            }
        }
    }
    
    func equipWeapon(){
        weaponLayer.removeAllChildren()
        weaponLayer.addChild(inventory.getEquipedWeapon())
        inventory.getEquipedWeapon().runIdleAnimation()
        inventory.getEquipedWeapon().position = CGPoint(x: 30, y: 30)
        
    }
    
    func equipArmour(){
        armourLayer.removeAllChildren()
        armourLayer.addChild(inventory.getEquipedArmour())
        // run idle here
    }
    
    
    // ANIMATION
    func runIdleAnimation(){
        self.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(texturesIdle, timePerFrame: 0.7)))
    }
    
    func runMovingAnimation(fast: Bool){
        if fast {
            self.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(texturesMoving, timePerFrame: 0.07)))
        } else {
            self.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(texturesMoving, timePerFrame: 0.1)))
        }
        
    }
    
    func runDeathAnimation(){
        self.runAction(SKAction.animateWithTextures(texturesDeath, timePerFrame: 0.1))
        
    }
    
    func runAttackAnimation(){
        self.runAction(SKAction.animateWithTextures(texturesAttack, timePerFrame: 0.1))
        inventory.getEquipedWeapon().runAttackAnimation()
    }
    
    func runJumpAnimation(){
        self.runAction(SKAction.animateWithTextures(texturesJump, timePerFrame: 0.1))
    }
    
    
    
    func showWeapon(){
        
    }
    
    func showArmour(){
        
    }
    
}
