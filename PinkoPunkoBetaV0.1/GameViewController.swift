//
//  GameViewController.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 5/07/2016.
//  Copyright (c) 2016 Sam. All rights reserved.
//

import UIKit
import SpriteKit

//, ContainerToMaster
class GameViewController: UIViewController {

    @IBOutlet weak var joyPad: UIView!
    var joypadVC: Joypad!
    var touchUIVC: TouchUI!
    var deathVC: Death!
    
    var deathScene: DeathScene!
    
    @IBOutlet weak var A: UIButton!
    @IBOutlet weak var B: UIButton!
    @IBOutlet weak var C: UIButton!
    
    private var levels = [String:GameScene]()

    @IBOutlet var skView: SKView!
    private var currScene: GameScene!
    
    private var hero: Hero!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set the button transparency
        A.alpha = 0.2
        B.alpha = 0.2
        C.alpha = 0.2
        
        // Configure the view.
        let skView = self.view as! SKView
        
        // our FPS
        skView.showsFPS = true
        
        // tells us how many nodes
        skView.showsNodeCount = true
        
        // this will show us the physical bodies that exist on the screen by outlining them (debug)
        skView.showsPhysics = true
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = true
        
        // create a Hero
        hero = Hero(initLevel: 1, initTexture: "Images/Hero/Character/Idle/MC_Idle_01", initSize: CGSize(width: 35.0, height:70.0))
        // make the hero size relative to the screen
        hero.name = "HERO"
        
        // testing WeaponX
        let weaponX = WeaponX()
        hero.pickUpItem(weaponX)
        
        //create all levels here --------------
        // NOTE: all levels must be named
        
        
        deathScene = DeathScene(initName: "rgkjnrkbjen", initSize: view!.bounds.size)
        
        let scene = GameScene(initName: "Level1", initHero: hero ,initSize: view.bounds.size, initPosition: CGPoint(x: 100, y: 100), joypadScene: joypadVC.currScene)
        levels[scene.name!] = scene
         
        let scene2 = GameScene(initName: "Level2", initHero: hero ,initSize: view.bounds.size, initPosition: CGPoint(x: 100, y: 100), joypadScene: joypadVC.currScene)
        levels[scene2.name!] = scene2
        
        // ".presentScene" method will display the scene
        // once presented, the skView will switch from rendering and displaying the content
        skView.presentScene(levels["Level1"]!, transition: SKTransition.fadeWithDuration(1))
        currScene = scene
        let enemy = Soldier(initLevel: 20)
        currScene.addEnemy(enemy, enemyPosition: CGPoint(x: 200, y: 100))
        
        
    }
    

    override func shouldAutorotate() -> Bool {
        return true
    }
    
    @IBAction func buttonA(sender: AnyObject) {
        print("GVC A")
        // if the hero is alive
        if hero.getHeroHealth() > 0 {
            currScene.attack()
        }
    }
    
    @IBAction func buttonB(sender: AnyObject) {
        print("GVC B")
        // if the hero is alive
        if hero.getHeroHealth() > 0 {
            currScene.jump()
        }
    }
    
    @IBAction func buttonC(sender: AnyObject) {
        print("GVC C")
        // if the hero is alive
        if hero.getHeroHealth() > 0 {
            let enemy = Soldier(initLevel: 3)
            currScene.addEnemy(enemy, enemyPosition: CGPoint(x: 300, y: 100))
            
            
        }
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        // if we've died and we need to enter the death scene
        if hero.getHeroHealth() <= 0 && !deathScene.getShowing() {
            print("death")
            // present the death scene
            skView.presentScene(deathScene!, transition: SKTransition.fadeWithDuration(1))
            
            // disable touch for the respective views to allow for touch to occur for the death scene
            joypadVC.currScene.userInteractionEnabled = false
            joypadVC.view.userInteractionEnabled = false
            touchUIVC.view.userInteractionEnabled = false
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        // if we're leaving the death scene and entering back into the currScene
        if deathScene!.getContinue() && hero.getHeroHealth() <= 0 {
            // present the curr scene
            skView.presentScene(currScene, transition: SKTransition.fadeWithDuration(1))
            hero.revive_hero()
            
            // re-enable touch for the respective views as we leave death view
            joypadVC.currScene.userInteractionEnabled = true
            joypadVC.view.userInteractionEnabled = true
            touchUIVC.view.userInteractionEnabled = true
        }
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    // setup conections for container views to current view
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "joypadSegue" {
            joypadVC = segue.destinationViewController as? Joypad
            joypadVC?.gameVC = self
            print("joypad segue")
            
        } else if segue.identifier == "touchUISegue" {
            touchUIVC = segue.destinationViewController as? TouchUI
            touchUIVC?.gameVC = self
            print("touchUI segue")
        }
    }
    
    
}
