//
//  DeathScene.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 14/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import SpriteKit


class DeathScene: SKScene {
    
    private var continueButton: SKNode!
    private var continuePressed: Bool! = false
    private var showing: Bool! = false
    
    // Constructor
    init(initName: String, initSize: CGSize){
        super.init(size: initSize)
        /* Set the scale mode to scale to fit the window */
        self.scaleMode = .AspectFill
        self.backgroundColor = UIColor.blackColor()
        
        /* Setup your scene here */
        // Create a simple red rectangle that's 100x44
        continueButton = SKSpriteNode(color: SKColor.redColor(), size: CGSize(width: 100, height: 44))
        // Put it in the center of the scene
        continueButton.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame));
        self.addChild(continueButton)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // first instance of the view
    override func didMoveToView(view: SKView) {
        print("showing death view")
        
        showing = true
        
    }
    
    // leaving the view
    override func willMoveFromView(view: SKView) {
        showing = false
        continuePressed = false
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print ("death scene touch began")
        
        // Loop over all the touches in this event
        for touch: AnyObject in touches {
            // Get the location of the touch in this scene
            let location = touch.locationInNode(self)
            // Check if the location of the touch is within the button's bounds
            if continueButton.containsPoint(location) {
                print("tapped!")
                continuePressed = true
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        
    }
    
    func getContinue() -> Bool {
        return continuePressed
    }
    
    func getShowing() -> Bool {
        return showing
    }
    
    
    
}