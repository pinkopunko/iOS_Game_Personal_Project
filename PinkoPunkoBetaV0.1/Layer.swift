//
//  Layer.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 7/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//
import SpriteKit

class Layer: SKNode {

    var layerVelocity = CGPointZero
    
    func update(delta:CFTimeInterval){
        // iterates through all the children of the node
        for child in children {
            // for every child, the updateGlobal will be called
            updateNodesGlobal(delta, childNode: child)
        }
    }
    
    func updateNodesGlobal(delta: CFTimeInterval, childNode: SKNode){
        let offset = layerVelocity * CGFloat(delta)
        childNode.position += offset
        updateNodes(delta, childNode: childNode)
        
        
    }
    
    func updateNodes(delta: CFTimeInterval, childNode: SKNode){
        // overridden by subclasses
    }
    
}