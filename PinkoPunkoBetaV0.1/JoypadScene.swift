//
//  JoypadScene.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 10/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import UIKit
import SpriteKit




class JoypadScene: SKScene, SKPhysicsContactDelegate {
    
    // for joypad
    private var ball: SKSpriteNode!
    private var pad: SKSpriteNode!
    private var stickActive: Bool = false
    private var joypadLayer = SKNode()
    
    
    override func didMoveToView(view: SKView) {
        //self.scaleMode = .AspectFill
        self.anchorPoint = CGPointMake(0.5, 0.5)
        addJoypad()
        addChild(joypadLayer)
    }
    
    
    
    //MARK: Joypad
    func addJoypad(){
        ball = SKSpriteNode(imageNamed: "Images/Joystick/ball")
        pad = SKSpriteNode(imageNamed: "Images/Joystick/pad")
        
//        ball.position = CGPoint(x: self.size.width/4, y: self.size.height/4)
        ball.position = CGPointZero
        pad.position = CGPointZero
        
        ball.anchorPoint = CGPointMake(0.5, 0.5)
        pad.anchorPoint = CGPointMake(0.5, 0.5)
        
        ball.size = CGSize(width: self.size.width/2, height: self.size.height/2)
        pad.size = CGSize(width: self.size.width, height: self.size.height)
        
        ball.zPosition = 10
        pad.zPosition = 9
        
        // makes it transparent
        // lower = more transparent
        ball.alpha = 0.4
        pad.alpha = 0.3
        
        joypadLayer.addChild(ball)
        joypadLayer.addChild(pad)
        
    }
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        for touch in (touches){
            let location = touch.locationInNode(self)
            // if we've touched within the joypad
            if CGRectContainsPoint(pad.frame, location) {
                stickActive = true
            } else {
                stickActive = false
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in (touches){
            
            // if we've started touching the screen from within the movement pad
            if (stickActive){
                // joypad config
                let location = touch.locationInNode(self)
                let vector = CGVector(dx: location.x - pad.position.x, dy: location.y - pad.position.y)
                let angle = atan2(vector.dy,vector.dx)
                //let degree = angle * CGFloat(180 / M_PI)
                //print( degree + 180 )
                
                let lengthX: CGFloat = pad.frame.size.width / 2
                let lengthY: CGFloat = pad.frame.size.height / 2
                
                let xDist: CGFloat = sin(angle - 1.57079633) * lengthX
                let yDist: CGFloat = cos(angle - 1.57079633) * lengthY
                
                let boundary = CGPointMake(pad.position.x - xDist, pad.position.y + yDist)
                
                // if touch is within the joypad
                if CGRectContainsPoint(pad.frame, location) && location.distanceTo(CGPointZero) <= boundary.distanceTo(CGPointZero){
                    
                    ball.position = location
                    
                // if you're touching outside the joypad
                } else {
                    ball.position = boundary
                }
                
//                print(" (\(ball.position.x), \(ball.position.y))  (x, y)" )
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        ball.position = CGPointZero
    }
    
    func getBallxPos() -> CGFloat{
        if stickActive {
//            print(ball.position.x)
            return ball.position.x
        } else {
            return 0.0
        }
    }
    
    func isActive() -> Bool{
        return stickActive
    }
    
}

