//
//  Joypad.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 10/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import UIKit
import SpriteKit

//protocol ContainerToMaster {
//}

class Joypad: UIViewController {
    
//    var containerToMaster:ContainerToMaster?
    var gameVC: GameViewController?
    var currScene:JoypadScene!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure the view.
        let skView = self.view as! SKView
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = true
        
        currScene = JoypadScene(size: skView.bounds.size)
        currScene.scaleMode = .Fill
        currScene.alpha = 0.5
        
        skView.presentScene(currScene)
    }
    
}