//
//  Soldier.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 21/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import SpriteKit


class Soldier: Enemy {
    
    init(initLevel: Int){
        
        super.init(initLevel: initLevel, initTexture: "Images/Enemy/Soldier/Idle/Soldier_01", initSize: CGSize(width: 35,height: 70), meleeType: true, magicType: false)
        
        // Idle animation
        for i in 1...2 {
            let imgName = "Images/Enemy/Soldier/Idle/Soldier_0\(i)"
            texturesIdle.append(SKTexture(imageNamed: imgName))
        }
        
        // Walking animation
        for i in 1...8 {
            let imgName = "Images/Enemy/Soldier/Idle/Soldier_walking_\(i)"
            texturesMoving.append(SKTexture(imageNamed: imgName))
        }
        
        // Death animation
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}