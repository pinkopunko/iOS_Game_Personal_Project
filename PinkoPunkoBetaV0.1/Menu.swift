//
//  Menu.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 7/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import SpriteKit

class Menu: UIViewController {

    @IBOutlet weak var start: UIButton!
    @IBOutlet weak var options: UIButton!
    
    @IBAction func startPressed(sender: AnyObject) {
    }
    
    
    @IBAction func optionsPressed(sender: AnyObject) {
    }
    
    // prepare for segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }
}
