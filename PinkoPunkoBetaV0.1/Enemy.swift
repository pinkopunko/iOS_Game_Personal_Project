//
//  Enemy.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 10/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import Foundation

import SpriteKit

class Enemy: SKSpriteNode {
    // MARK: Fields
    internal var enemy_level: Int!
    internal var enemy_exp_toNextLvl: Double = 100.0
    
    internal var enemy_skills: Array<Skill> = Array<Skill>()
    
    internal var enemy_hitpoints_max: Int = 100
    internal var enemy_hitpoints_curr: Int = 100
    
    internal var enemy_manapoints_max: Int = 100
    internal var enemy_manapoints_curr: Int = 100
    
    internal var enemy_melee_dmg_max: Int = 5
    internal var enemy_melee_dmg_min: Int = 2
    
    internal var enemy_magic_dmg_max: Int = 5
    internal var enemy_magic_dmg_min: Int = 2
    
    internal var enemy_att_speed: Double = 1
    
    internal var enemy_melee_def: Int = 0
    internal var enemy_magic_def: Int = 0
    
    internal var enemy_dead: Bool = false
    
    internal var inventory: Inventory!
    
    internal var texturesIdle = [SKTexture]()
    internal var texturesMoving = [SKTexture]()
    internal var texturesDeath = [SKTexture]()
    internal var texturesAttack = [SKTexture]()
    
    init(initLevel: Int, initTexture: String, initSize: CGSize, meleeType: Bool, magicType: Bool){
        
        let enemyImage = SKTexture(imageNamed: initTexture)
        super.init(texture: enemyImage, color: UIColor.whiteColor(), size: initSize)
        
        self.addPhysicsBodyWithSize(self.size, gravity: true, dynamic: true, rotation: false, friction: 0.1, restitution: 0.0, categoryBitMask: enemyCategory, contactTestBitMask: groundCategory | heroCategory, collisionBitMask: groundCategory)
        
        if meleeType {
            self.enemy_melee_dmg_min += initLevel * 5
            self.enemy_melee_dmg_max += initLevel * 6
        }
        if magicType {
            self.enemy_magic_dmg_min += initLevel * 5
            self.enemy_magic_dmg_max += initLevel * 6
        }
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: Functions
    func attack(){
        
    }
    
    // show an animation. the UI will take care of position of character
    func jump(){
        
    }
    
    // show an animation. the UI will take care of position of character
    func move(){
        
    }
    
    func checkHealth(){
        
    }
    
    
    
    func performSkill(){
        
    }

    
    final func getEnemyHealth() -> Int{
        return enemy_hitpoints_curr
    }
    
    
    // takes damage
    // change to damage taken animation
    // check if you die
    final func takeDamage(damageDone: Int){
        self.enemy_hitpoints_curr -= damageDone
        
    }
    
    final func getEnemyDmg() -> Int {
        return (random() % (enemy_melee_dmg_max - enemy_melee_dmg_min)) + enemy_melee_dmg_min
    }
    
    // enemy has died
    // change to death animation
    final func set_enemy_dead (){
        self.enemy_dead = true
    }

}