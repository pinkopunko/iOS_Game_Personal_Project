//
//  WeaponX.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 22/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import SpriteKit

class WeaponX: Weapon {
    
    init (){
        super.init(name: "WeaponX", texture: "Images/Items/Weapons/Weapon X/Weapon X 01", size: CGSize(width: 25,height: 70), levelRequirement: 5, itemLevel: 5, rarity: 10)
        
        var imgName: String
        
        // Idle animation
        for i in 1...2 {
            let imgName = "Images/Items/Weapons/Weapon X/Weapon X 0\(i)"
            texturesIdle.append(SKTexture(imageNamed: imgName))
        }
        
        // Attacking animation
        imgName = "Images/Items/Weapons/Weapon X/Weapon X 03"
        texturesAttack.append(SKTexture(imageNamed: imgName))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func runIdleAnimation(){
        self.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(texturesIdle, timePerFrame: 0.3)))
    }
    
    override func runAttackAnimation(){
        self.runAction(SKAction.animateWithTextures(texturesAttack, timePerFrame: 0.3))
        self.position = CGPoint(x: -15,y: 50)
        self.zRotation = 90 * 3.14 / 180
        self.runAction(SKAction.rotateByAngle(-230 * 3.14 / 180, duration: 0.3))
        self.runAction(SKAction.moveTo(CGPoint(x: 30, y: -20), duration: 0.3))
    }
}