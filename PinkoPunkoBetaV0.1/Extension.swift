//
//  Extension.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 7/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import SpriteKit

extension SKSpriteNode {
    func addPhysicsBodyWithSize(size:CGSize, gravity: Bool, dynamic: Bool, rotation: Bool, friction: CGFloat, restitution: CGFloat, categoryBitMask: UInt32, contactTestBitMask: UInt32, collisionBitMask: UInt32){
        
        // sets the actual size of the physics body
        self.physicsBody = SKPhysicsBody(rectangleOfSize: size)
        // decides if it's affected by grav
        self.physicsBody?.affectedByGravity = gravity
        
        // if the body is affected by other elements/contact
        self.physicsBody?.dynamic = dynamic
        
        // able to rotate?
        self.physicsBody?.allowsRotation = rotation
        
        // sets the friction from vales b/t 0.0 to 1.0
        self.physicsBody?.friction = friction
        
        // determines how bouncy from values b/t 0.0 to 1.0
        self.physicsBody?.restitution = restitution
        
        
        self.physicsBody?.categoryBitMask = categoryBitMask
        
        // determines if the elements did infact collide
        self.physicsBody?.contactTestBitMask = contactTestBitMask
        
        // determines which elements collide with each other
        self.physicsBody?.collisionBitMask = collisionBitMask
    }
}