//
//  GameSceneList.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 5/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import SpriteKit

class GameSceneList: UIViewController {
    
    //private var listOfLevels: Link
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let scene = GameScene(size: view.bounds.size)
        
        // Configure the view.
        let skView = self.view as! SKView
        
        // our FPS
        skView.showsFPS = true
        
        // tells us how many nodes
        skView.showsNodeCount = true
        
        // this will show us the physical bodies that exist on the screen by outlining them (debug)
        skView.showsPhysics = true
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = true
        
        /* Set the scale mode to scale to fit the window */
        scene.scaleMode = .AspectFill
        
        scene.name = "level1"
        
        // ".presentScene" method will display the scene
        // once presented, the skView will switch from rendering and displaying the content
        skView.presentScene(scene)
        // this can be paused via:
        //skView.paused = true
        
        /*
         A scene calculates its updates in the following order (happens b/t each frame):
         - first the "-update:" method is called.
         - SKScene evaluates all actions for the children
         - "-didEvaluateActions"
         - SKScene simulates physics
         - "-didSimulatePhysics"
         - SKScene applies contraints
         - "-didApplyConstraints"
         - "-didFinishUpdate"
         - SKScene renders the scene
         */
        
    }
    
    
    
}
