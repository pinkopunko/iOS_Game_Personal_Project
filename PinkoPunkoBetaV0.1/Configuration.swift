//
//  Configuration.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 7/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import SpriteKit

// we add categories for the different types of bodies that exist for contact
let heroCategory: UInt32 =    0x1 << 0
let groundCategory: UInt32 =    0x1 << 1
let enemyCategory: UInt32 =  0x1 << 2
let scoreCategory: UInt32 =     0x1 << 3

// we need the value of the screen height and screen width

//scale returns:
// 1.0 for non-retina displays
// 2.0 for retina displays
let scale = UIScreen.mainScreen().scale
// scale is used to return the correct screen height and width as shown below
let screenWidth = UIScreen.mainScreen().bounds.width * scale
let screenHeight = UIScreen.mainScreen().bounds.height * scale