//
//  TouchUI.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 7/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import UIKit
import SpriteKit

class TouchUI: UIViewController {
    
    var gameVC: GameViewController?
    

    @IBAction func rotate(sender: AnyObject) {
        print("TouchUI Rotate")
    }
    @IBAction func pinch(sender: AnyObject) {
        print("TouchUI Pinch")
    }
    @IBAction func singleTap(sender: AnyObject) {
        print("TouchUI SingleTap")
    }
    @IBAction func swipeRight(sender: AnyObject) {
        print("TouchUI SwipeRight")
    }
    @IBAction func swipeLeft(sender: AnyObject) {
        print("TouchUI SwipeLeft")
    }
    
    @IBAction func panUp(sender: AnyObject) {
        print("TouchUI Pan")
    }
    @IBAction func upGesture(sender: UISwipeGestureRecognizer) {
        print ("TouchUI Up")
    }
    
    @IBAction func downGesture(sender: UISwipeGestureRecognizer) {
        print ("TouchUI Down")
    }
    
    @IBAction func doubleTap(sender: AnyObject) {
        print("TouchUI DoubleTap")
    }
    
    @IBAction func tfSingleTap(sender: AnyObject) {
        print("TouchUI TFSingleTap")
    }
    
    @IBAction func longPress(sender: UILongPressGestureRecognizer) {
        print("TouchUI Long Press")
    }
    
    @IBAction func tfLongPress(sender: UILongPressGestureRecognizer) {
        print("TouchUI TFLong Press")
    }
    
    
    
    
}