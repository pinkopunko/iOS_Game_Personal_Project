//
//  Inventory.swift
//  PinkoPunkoBetaV0.1
//
//  Created by BadBoy17 on 6/07/2016.
//  Copyright © 2016 Sam. All rights reserved.
//

import Foundation
import SpriteKit

class Inventory {
    // max is a set amount of items. can change throughout the course of the game
    
    private var maxInventory: Int = 30
    private var Weapons = [Weapon]()
    private var Armours = [Armour]()
    private var weaponEquiped: Weapon!
    private var armourEquiped: Armour!
    
    init (){
    }
    
    func getEquipedWeapon() -> Weapon{
        return weaponEquiped
    }
    
    func getEquipedArmour() -> Armour{
        return armourEquiped
    }
    
    func addWeapon(weapon: Weapon){
        Weapons.append(weapon)
        if weaponEquiped == nil {
            weaponEquiped = weapon
        }
    }
    
    func addArmour(armour: Armour){
        Armours.append(armour)
        if armourEquiped == nil {
            armourEquiped = armour
        }
    }
    
    func equipWeapon(name: String){
        for weapon in Weapons {
            if name == weapon.name {
                weaponEquiped = weapon
            }
        }
        
    }
    
    func equipArmour(name: String){
        for armour in Armours {
            if name == armour.name {
                armourEquiped = armour
            }
        }
    }
    
    func increaseMaxInventory(increaseBy: Int){
        assert(increaseBy > 0)
        maxInventory += increaseBy
    }
}